## Icon libraries for OpenHAB2 ##

This repo contains additional icons for OpenHAB2. You can clone this icons to the OpenHAB2 iconss directory.

---

## Basics for OpenHAB2 icons ##

OpenHAB2 comes with a set of predefined icons. The default file format is SVG. You can also use PNG files, but you have to configure OpenHAB2 for using this filetypes.

---

## Content of this icons library ##

You can find directories with a specific name, e.g. "oh1". This directory contains two subdirectories "PNG" and "SVG", which contains the same icons, but in different file formats.

---

## Clone or download the repository ##

If you are using Git you can clone this directory to a local repo. If you are not using Git, just download the repo as ZIP file and deflate it in a directory of your choice.

---